from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import LocationVO, Hat
from django.views.decorators.http import require_http_methods
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {
            "location": o.location.closet_name,
        }


class HatsDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "picture_url",
        "location",
    ]

    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, hat_vo_id=None):
    if request.method == "GET":
        if hat_vo_id is not None:
            hats = Hat.objects.filter(location=hat_vo_id)
        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(
                import_href=f"/api/locations/{location_href}/"
            )
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
            )
        hat = Hat.objects.create(**content)

        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hats_detail(request, hat_id):
    if request.method == "GET":
        details = Hat.objects.get(id=hat_id)
        return JsonResponse(
            details,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=hat_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Hat.objects.filter(id=hat_id).update(**content)

        hat = Hat.objects.get(id=hat_id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
