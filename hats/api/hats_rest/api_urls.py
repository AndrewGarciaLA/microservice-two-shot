from django.urls import path
from . import views

urlpatterns = [
    path("hats/", views.api_list_hats, name="api_list_hats"),
    path("hats/<int:hat_id>/", views.api_hats_detail, name="api_hats_detail"),
]
