import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()
# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVO


def get_locations():
    hat_url = "http://wardrobe-api:8000/api/locations/"
    response = requests.get(hat_url)
    content = json.loads(response.content)
    print(content)

    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            # creating a new hatVO unless it exists, then you update
            import_href=location["href"],
            defaults={
                "closet_name": location["closet_name"],
                "shelf_number": location["shelf_number"],
                "section_number": location["section_number"],
            },
        )
    # for location in content:
    # if location.get("resource_type") == "Hat":
    # hat_data = {
    # "fabric": location.get("fabric"),
    # "style_name": location.get("style_name"),
    # "color": location.get("color"),
    # "picture_url": location.get("picture_url"),
    # "location": location.get("name"),
    # }
    # Hat.objects.create(**hat_data)


def poll():
    while True:
        print("Hats poller polling for data")
        try:
            get_locations()
        except Exception as e:
            print("error", e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
