import React, { useEffect, useState } from 'react';

function CreateHatForm(props) {
    const [locations, setLocations] = useState([]);
    const [styleName, setStyleName] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [pictureURL, setPicture] = useState('');
    const [selectedLocation, setSelectedLocation] = useState('');


    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }


    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }


    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }


    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }


    const handleSelectedLocationChange = (event) => {
        const value = event.target.value;
        setSelectedLocation(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureURL;
        data.location = selectedLocation;

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();

            setStyleName('');
            setFabric('');
            setColor('');
            setPicture('');
            setSelectedLocation('');

            props.fetchHats();
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Hat</h1>
                        <form onSubmit={handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleStyleNameChange} placeholder="style-name" value={styleName} required type="text" id="style_name" name="style_name" className="form-control" />
                                <label htmlFor="style_name">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFabricChange} placeholder="fabric" value={fabric} required type="text" id="fabric" name="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="Color" value={color} required type="text" id="color" name="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureChange} placeholder="picture-URL" value={pictureURL} required type="url" id="picture_url" name="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="location" className="form-label">Location</label>
                                <select onChange={handleSelectedLocationChange} required id="location" value={selectedLocation} name="location" className="form-select">
                                    <option defaultValue="">Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.closet_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateHatForm;
