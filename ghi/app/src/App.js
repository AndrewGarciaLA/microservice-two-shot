import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ApiDataRenderer from './shoes';
import Nav from './Nav';
import HatList from './HatList';
import CreateHatForm from './CreateHatForm';

function App() {
  const [hats, setHats] = useState([]);

  const fetchHats = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
<<<<<<< HEAD
          <Route path='/shoes' element={<ApiDataRenderer />} />
=======
          <Route path="/hats" element={<HatList hats={hats} fetchHats={fetchHats} />} />
          <Route path="/hats/new" element={<CreateHatForm fetchHats={fetchHats} />} />
>>>>>>> main
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
