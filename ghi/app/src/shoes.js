import React, { useState, useEffect } from "react";

function ShoePage() {
    const [data, setData] = useState([])

    const apiGet = () => {
        fetch("http://localhost:8100/api/bins")
        .then((response) => response.json())
        .then((json) => {
            setData(json);
        });
    };

    useEffect(() => {
        apiGet();
    }, [])

    let stringJson = JSON.stringify(data)
    let obj = JSON.parse(stringJson)

    console.log(obj.bins[0].closet_name)

    return (
        <div>
            <h1>Api Call</h1>
            
        </div>
    )
}

export default ShoePage;