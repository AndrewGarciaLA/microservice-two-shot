import React from "react";

function HatList(props) {
    const handleDelete = async (hatId) => {
        const confirmDelete = window.confirm("Are you sure you want to delete this hat?");
        if (!confirmDelete) {
            return;
        }
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, {
            method: 'DELETE',
        });
        if (response.ok) {
            props.fetchHats();
        } else {
            console.error(response);
        }
    };
    return (
    <div>
        <h1></h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Picture</th>
            <th>Style</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        {props.hats.map(hat => {
        return (
        <tr key={hat.id}>
            <td>{hat.picture_url && <img src={hat.picture_url} alt={hat.name}  style={{ width: "50px", height: "50px" }}/>}</td>
            <td>{hat.style_name}</td>
            <td>{hat.fabric}</td>
            <td>{hat.color}</td>
            <td>{hat.location}</td>
            <td>
                <button className="btn btn-danger" onClick={() => handleDelete(hat.id)}>
                    Delete
                </button>
            </td>
        </tr>
        );
        })}
        </tbody>
    </table>
    </div>
    );
}

export default HatList;
