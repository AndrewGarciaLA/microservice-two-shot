from django.db import models
from django.urls import reverse

# Create your models here.
class ShoeVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    shoe_name = models.CharField(max_length=200, null=True)

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()

    def get_api_url(self):
        return reverse("api_bin", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name}"

    class Meta:
        ordering = ("manufacturer", "model_name")