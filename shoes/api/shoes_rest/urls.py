from django.urls import path
from .views import *


urlpatterns = [
    path("manufacturer/", api_manufacturer, name="api_manufacturer"),
    path("model/<int:pk>/", api_models, name="api_models")
]