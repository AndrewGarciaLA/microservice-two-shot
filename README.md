# Wardrobify

Team:

<<<<<<< HEAD
* Person 1 - Which microservice?
* Tyler Curtis - Shoes
=======
- Andrew Garcia- Hats
- Tyler Curtis- Shoes
>>>>>>> main

## Design

## Shoes microservice

I will be creating a polling service that will pull data into the bin in the wardrobe microservice. Then I will render the data from bin onto the webpage using react components.

## Hats microservice

For my hats microservice, I made two models.
A LocationVO model that gets the location of the wardrobe for hats,
and a Hat model that shows the hat and it's details.

Through the use of the location model in the wardrobe api,
I was able to extract the necessary fields in order to integrate it
within the locationVO model in the hats_rest api.
